ruby-uuid (2.3.9-4) unstable; urgency=medium

  * Re-upload to unstable.

 -- Utkarsh Gupta <utkarsh@debian.org>  Sat, 08 Mar 2025 06:58:34 +0530

ruby-uuid (2.3.9-3) experimental; urgency=medium

  * Team upload.
  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files out of the source package

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

  [ Vivek K J ]
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Remove XB-Ruby-Versions (Not needed)
  * Use ${ruby:Depends} instead of ruby-interpreter
  * Add patch to fix FTBFS with mocha 2 (Closes: #1090880)

 -- Vivek K J <vivekkj@disroot.org>  Mon, 23 Dec 2024 22:42:13 +0530

ruby-uuid (2.3.9-2) UNRELEASED; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 08:10:26 +0530

ruby-uuid (2.3.9-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.3.9
  * Bump Standards-Version to 4.3.0
  * Move debian/watch to gemwatch.debian.net
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Fri, 11 Jan 2019 23:14:58 +0530

ruby-uuid (2.3.8+debian-1) unstable; urgency=medium

  * New upstream release.
  * Remove bin using gbp.conf instead of patch.

 -- Pirate Praveen <praveen@debian.org>  Fri, 28 Aug 2015 21:33:36 +0530

ruby-uuid (2.3.7-3) unstable; urgency=medium

  * Add ruby-mocha to test depends.
  * Enable a simple require test.

 -- Pirate Praveen <praveen@debian.org>  Mon, 11 May 2015 15:16:38 +0530

ruby-uuid (2.3.7-2) unstable; urgency=medium

  * Add CC-BY-SA as dual license
  * Re-upload to unstable

 -- Pirate Praveen <praveen@debian.org>  Mon, 27 Oct 2014 18:40:46 +0530

ruby-uuid (2.3.7-1) experimental; urgency=medium

  * Initial release (Closes: #763359)

 -- Pirate Praveen <praveen@debian.org>  Wed, 22 Oct 2014 12:06:10 +0530
